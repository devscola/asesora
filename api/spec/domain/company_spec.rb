require_relative '../../system/domain/company'
require 'securerandom'

describe 'Company' do
  it 'identifies with an ID' do
    generated_id = 'secure_random_id'
    allow(SecureRandom).to receive(:uuid).and_return(generated_id)
    company = Domain::Company.with('name', 'cif', 'employees', 'cnae', 'cp')

    id = company.identify

    expect(id).to eq(generated_id)
  end

  it 'can be generated from document' do
    document = {
      'cif' => 'cif',
      'name' => 'name',
      'employees' => 'employees',
      'cnae' => 'cnae',
      'cp' => 'cp',
      'id' => 'id'
    }

    company = Domain::Company.from_document(document)

    expect(company.serialize).to eq(document)
  end

  it 'can be create with specific information' do
    information = {
      'cif' => 'cif',
      'name' => 'name',
      'employees' => 'employees',
      'cnae' => 'cnae',
      'cp' => 'cp',
      'id' => 'id'
    }

    company = Domain::Company.with(
      information['name'],
      information['cif'],
      information['employees'],
      information['cnae'],
      information['cp'],
      information['id']
    )

    expect(company.serialize).to eq(information)
  end
end
