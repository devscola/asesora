require_relative '../../system/domain/attachment'

describe 'Attachment' do
  it 'can be create with specific information' do
    attachment_keys = ['id', 'name']
    document = { 'id' => 'id', 'name' => 'name' }

    attachment = Domain::Attachment.with(
      document['name'],
      document['id']
    )

    expect_include_keys_in(attachment.serialize, attachment_keys)
  end

  def expect_include_keys_in(dictionary, keys)
    keys.each do |key|
      expect(dictionary).to have_key(key)
    end
  end
end
