require 'json'
require 'rack/test'

require_relative './fixtures/asesora_with_fixtures'


describe 'Topics' do

  include Rack::Test::Methods

  def app
    AsesoraWithFixtures
  end

  it 'retrieves complete catalog' do
    post '/api/topics', {}
    catalog = JSON.parse(last_response.body)

    expect(catalog['data']['name']).to eq('topics')
    expect(catalog['data']['content'].size).to eq(69)
  end
end
