require_relative  '../../system/services/attachments/service'
require_relative '../../system/infrastructure/clients'
require_relative './../fixtures/fixtures'

describe 'Attachment service' do
  before(:each) do
    allow(Infrastructure::Clients).to receive(:sftp).and_return(StubSFTP)
    Fixtures.new.drop_attachments
  end

  after(:each) do
    Fixtures.new.drop_attachments
  end

  it "create attachment in collection" do
    file = {"file"=>"attach_file", "name"=>"file.csv"}
    subject_id = "123456789"

    attachment = Attachments::Service.create(file, subject_id)

    expect(attachment["name"]).to eq(file['name'])
    expect(attachment["subject_id"]).to eq(subject_id)
    expect(attachment["id"]).not_to be_nil
  end

  it "retrieve attachments" do
    first_files = [{"file"=>"attach_file", "name"=>"file.csv"}, {"file"=>"attach_file_one", "name"=>"file.csv"}]
    second_files = [{"file"=>"attach_file", "name"=>"file.csv"}, {"file"=>"attach_file_one", "name"=>"file.csv"}]
    subject_id_one = "123456789"
    subject_id_two = "123456788"
    create_attachments(subject_id_one, first_files)
    create_attachments(subject_id_two, second_files)

    first_attachments = Attachments::Service.retrieve(subject_id_one)
    second_attachments = Attachments::Service.retrieve(subject_id_two)

    expect(first_attachments[0]["subject_id"]).to eq(subject_id_one)
    expect(first_attachments.count).to eq(2)
    expect(second_attachments[1]["name"]).to eq("file.csv")
  end

  it "deletes subject's attachments from collection" do
    files = [{"file"=> "attach_file", "name"=>"file.txt"},{"file"=> "attach_another_file", "name"=>"another_file.txt"}]
    subject_id = "123"
    create_attachments(subject_id, files)

    Attachments::Service.delete_for(subject_id)

    attachments = Attachments::Service.retrieve(subject_id)
    expect(attachments.count).to eq(0)
  end

  def create_attachments(subject_id, files)
    files.map do |file|
      Attachments::Service.create(file, subject_id)
    end
  end

end

class StubSFTP
  class << self
    def upload(name, file)
    end

    def delete(name)
    end

    def download(path, filename)
    end
  end
end

