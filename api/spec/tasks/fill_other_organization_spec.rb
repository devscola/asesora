require_relative '../../system/infrastructure/clients'
require_relative '../../tasks/fill_other_organization'

describe 'Fill Other Organization' do
  before(:each) do
    drop_collection
  end

  after(:all) do
    drop_collection
  end

  it 'fills solicitudes other organization with empty' do
    create_solicitude_without_other_organization

    FillOtherOrganization.do()

    solicitude = solicitudes.first
    expect(solicitude['organization']).not_to be_nil
  end

  def create_solicitude_without_other_organization
    solicitude = { 'creation_moment' => '123456789' }

    client[:solicitudes].insert_one(solicitude)
  end

  def solicitudes
    client[:solicitudes].find()
  end

  def drop_collection
    client[:solicitudes].drop()
  end

  def client
    @client ||= Infrastructure::Clients.mongo

    @client
  end
end
