require_relative '../../system/infrastructure/clients'
require_relative './sftp_client_test'

describe 'Clients' do
  context 'SFTP' do
    before(:each) do
      @client_test = STFPClientTest.new
      @client_test.drop
    end

    after(:each) do
      @client_test.delete(file_name)
    end

    after(:all) do
      STFPClientTest.new.drop
    end

    it 'uploads a file' do
      client = Infrastructure::Clients.sftp

      client.upload(file_name, base64_content)

      files = @client_test.list_files
      expect(files).to include(file_name)
    end

    it 'deletes a file' do
      client = Infrastructure::Clients.sftp
      client.upload(file_name, base64_content)

      client.delete(file_name)

      files = @client_test.list_files
      expect(files).not_to include(file_name)
    end

    it 'downloads a file' do
      path = './spec/infrastructure/downloads/'
      client = Infrastructure::Clients.sftp
      client.upload(file_name, base64_content)

      client.download(path + 'file_name', file_name)

      files = Dir.entries(path)
      expect(files).to include(file_name)
      File.delete(path + file_name)
    end

    def file_name
      'file_name'
    end

    def base64_content
      "SG9sYSBsb2xv"
    end
  end
end
