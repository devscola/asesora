require 'json'
require 'rack/test'

require_relative './fixtures/asesora_with_fixtures'


describe 'Source_formats' do

  include Rack::Test::Methods

  def app
    AsesoraWithFixtures
  end

  it 'retrieves complete catalog' do
    post '/api/source_formats', {}
    catalog = JSON.parse(last_response.body)

    expect(catalog['data']['name']).to eq('source_formats')
    expect(catalog['data']['content'].size).to eq(5)
  end
end
