require_relative '../../infrastructure/clients'
require_relative '../../domain/subject'

require 'i18n'
require 'json'

module Subjects
  class Collection
    class << self
      def create(subject)
        serialized = subject.serialize()

        document = MongoClient.create(serialized)
        Domain::Subject.from_document(document)
      end

      def update(subject, id)
        serialized = subject.serialize()

        document = MongoClient.update(serialized, id)

        return if document.nil?

        Domain::Subject.from_document(document)
      end

      def retrieve(id)
        subject = MongoClient.find(id)
        return subject if subject == nil
        Domain::Subject.from_document(subject)
      end

      def delete(id)
        MongoClient.delete(id)
      end

      def all_by(solicitude_id)
        subjects = MongoClient.all_by(solicitude_id)

        Domain::List.from_document(subjects, Domain::Subject)
      end

      def filter_by(criterial)
        subjects = MongoClient.filter_by(criterial)

        Domain::List.from_document(subjects, Domain::Subject)
      end

      def remove_all_by_solicitude(id)
        MongoClient.delete_by({"solicitude_id": id})
      end

      private

    class MongoClient
      class << self
        def create(descriptor)
          client[:subjects].insert_one(descriptor)
          descriptor
        end

        def all_by(solicitude_id)
          client[:subjects].find({"solicitude_id": solicitude_id}).sort({ "numeration": -1 })
        end

        def filter_by(criterial)
          all_subject = client[:subjects].find()
          result = []
          simplify_criterial = simplify(criterial)
          all_subject.each do |subject|
            subject.each_with_index do |value, index|
              unless is_invalid?(value)
                parsed_value = value.last.to_json
                subject_value = simplify(parsed_value)
                if subject_value.include?(simplify_criterial)
                  result << subject
                end
              end
            end
          end
          result
        end

        def update(subject, id)
          client[:subjects].find_one_and_replace({ "id": id }, subject, :return_document => :after)
        end

        def find(id)
          client[:subjects].find({"id": id}).first
        end

        def delete(id)
          client[:subjects].delete_one({"id": id})
        end

        def delete_by(condition)
          client[:subjects].find(condition).delete_many

          nil
        end

        private

          def is_invalid?(value)
            value.last.class == BSON::ObjectId || value.last.nil?
          end

          def simplify(value)
            I18n.transliterate(value.downcase)
          end

          def client
            @client ||= Infrastructure::Clients.mongo
          end
        end
      end
    end
  end
end
