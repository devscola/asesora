require_relative '../organizations/service'


module Validations
  class Service
    def self.show_owner?(owner, logged, isCoordinator)
      return true if belongsToIstas(logged)
      return true if (belongsToSameDomain(owner, logged) && isCoordinator)

      return false
    end

    def self.belongsToSameDomain(owner, logged)
      return false if owner.nil?
      return false if logged.nil?
      domainOwner = ::Organizations::Service.domain_in(owner)
      domainLogged = ::Organizations::Service.domain_in(logged)
      return (domainOwner == domainLogged)
    end

    def self.belongsToIstas(logged)
      return logged.include?("istas.ccoo.es")
    end
  end
end
