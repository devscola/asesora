require 'net/sftp'
require 'mongo'
require 'base64'

module Infrastructure
  class Clients
    def self.mongo
      mongo_uri = ENV['MONGODB_URI']
      mongo_user = ENV['MONGODB_USER']
      mongo_pssw = ENV['MONGODB_PSSW']
      ddbb_name = ENV['DDBB_NAME']
      Mongo::Logger.logger.level = Logger::INFO

      Mongo::Client.new(mongo_uri,
        {
          max_pool_size: 10,
          user: mongo_user,
          password: mongo_pssw,
          database: ddbb_name
        }
      )
    end

    def self.sftp
      SFTP.new
    end

    class SFTP
      SFTP_PATH = '/files/'

      def upload(name, file)
        client do |sftp|
          content = StringIO.new(Base64.decode64(file))

          sftp.upload!(content, SFTP_PATH + name)
        end
      end

      def delete(file)
        client do |sftp|
          sftp.remove(SFTP_PATH + file)
        end
      end

      def download(path, filename)
        client do |sftp|
          sftp.download!(SFTP_PATH + filename, path)
        end
      end

      def client(&block)
        host = ENV['SFTP_HOST']
        user = ENV['SFTP_USER']
        config = {
          :password => ENV['SFTP_PASSWORD'],
          :port => ENV['SFTP_PORT']
        }

        Net::SFTP.start(host, user, config) do |sftp|
          block.call(sftp)
        end
      end
    end

    class SFTPLogger
      def on_open(uploader, file)
        puts "[SFTP Service] starting upload: #{file.local} -> #{file.remote} (#{file.size} bytes)"
      end

      def on_put(uploader, file, offset, data)
        puts "[SFTP Service] writing #{data.length} bytes to #{file.remote} starting at #{offset}"
      end

      def on_close(uploader, file)
        puts "[SFTP Service] finished with #{file.remote}"
      end

      def on_mkdir(uploader, path)
        puts "[SFTP Service] creating directory #{path}"
      end

      def on_finish(uploader)
        puts "[SFTP Service] all done!"
      end
    end
  end
end
