require_relative '../services/attachments/service'
require_relative '../services/solicitudes/service'
require_relative '../services/applicant/service'
require_relative '../services/subjects/service'

module Actions
  class DeleteSolicitude
    class << self
      def do(id:)
        solicitude = ::Solicitudes::Service.retrieve(id)
        return "500" if solicitude == {}
        applicant_id =  solicitude['applicant']
        delete_applicant_orphans(applicant_id)
        company_id =  solicitude['company']
        delete_company_orphans(company_id)
        delete_attachments(id)
        delete_subjects_belonging(id)

        ::Solicitudes::Service.delete(id)
      end

      private

      def delete_attachments(solicitude)
        subjects = ::Subjects::Service.all_by(solicitude)

        subjects.each do |subject|
          ::Attachments::Service.delete_for(subject['id'])
        end
      end

      def delete_applicant_orphans(applicant_id)
        criteria_applicant = {"applicant": applicant_id}
        if only_one_solicitude_in(criteria_applicant)
          ::Applicant::Service.delete(applicant_id)
        end
      end

      def delete_company_orphans(company_id)
        return if company_id.nil? || company_id == ""
        criteria_company = {"company": company_id}
        if only_one_solicitude_in(criteria_company)
          ::Companies::Service.delete(company_id)
        end
      end

      def only_one_solicitude_in(criteria)
        all_solicitudes = ::Solicitudes::Service.all_by(criteria)
        return (all_solicitudes.length == 1)
      end

      def delete_subjects_belonging(id)
        ::Subjects::Service.remove_all_by_solicitude(id)
      end
    end
  end
end
