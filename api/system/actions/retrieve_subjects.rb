require_relative '../services/subjects/service'

module Actions
  class RetrieveSubjects
    def self.do(solicitude_id:)
      created_subjects = ::Subjects::Service.all_by(solicitude_id).each do |subject|
        attachments = ::Attachments::Service.retrieve(subject['id'])
        subject["files"] = attachments
      end
      created_subjects
    end
  end
end
