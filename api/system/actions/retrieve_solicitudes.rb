require_relative '../services/organizations/service'
require_relative '../services/solicitudes/service'
require_relative '../services/applicant/service'
require_relative '../services/companies/service'
require_relative '../services/subjects/service'
require_relative './solicitude_presenter'


module Actions
  class RetrieveSolicitudes
    class << self
      def do(*_)
        solicitudes = ::Solicitudes::Service.all

        fill(solicitudes)
      end

      def do_by_domain(user_id:)
        domain = cut_domain(user_id)
        solicitudes = ::Solicitudes::Service.all_by_domain(domain)

        fill(solicitudes)
      end

      def do_companies(criteria, user_id)
        companies = ::Companies::Service.all(criteria)

        select_by_domain(user_id, companies)
      end

      def do_domain_less(user:)
        solicitudes = ::Solicitudes::Service.all_by_domain_less(user)

        fill(solicitudes)
      end


      def do_applicants(criteria, user_id)
        applicants = ::Applicant::Service.all_by(criteria)

        select_by(user_id, applicants)
      end

      private

      def fill(solicitudes)
        solicitudes.map do |solicitude|
          company = ::Companies::Service.retrieve(solicitude['company'], solicitude['edition_moment'])
          applicant = ::Applicant::Service.retrieve(solicitude['applicant'])
          subjects = ::Subjects::Service.all_by(solicitude['creation_moment'])

          SolicitudePresenter
            .new(solicitude, company, applicant, subjects)
            .serialize(format: :short)
        end
      end

      def applicants_in(solicitudes)
        solicitudes.map do |solicitude|
          solicitude['applicant']
        end
      end

      def companies_in(solicitudes)
        solicitudes.map do |solicitude|
          solicitude['company']
        end
      end

      def select_by(user_id, all_applicants)
        solicitudes = ::Solicitudes::Service.all_by_user(user_id)
        applicants = applicants_in(solicitudes)
        all_applicants.select{|applicant| applicants.include?(applicant['id'])}
      end

      def select_by_domain(user_id, all_companies)
        domain = cut_domain(user_id)
        solicitudes = ::Solicitudes::Service.all_by_domain(domain)
        companies = companies_in(solicitudes)
        all_companies.select{|company| companies.include?(company['id'])}
      end

      def cut_domain(email)
        domain = ::Organizations::Service.domain_in(email)

        domain
      end
    end
  end
end
