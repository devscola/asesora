require_relative '../services/solicitudes/service'
require_relative 'solicitude_presenter'

module Actions
  class RetrieveAllSolicitudesByUser
    def self.do(user_id:, domain:)
      solicitudes = ::Solicitudes::Service.all_by_user(user_id)
      solicitudes.map do |solicitude|
        company = ::Companies::Service.retrieve(solicitude['company'], solicitude['edition_moment'])
        applicant = ::Applicant::Service.retrieve(solicitude['applicant'])
        subjects = ::Subjects::Service.all_by(solicitude['creation_moment'])

        SolicitudePresenter.new(solicitude, company, applicant, subjects).serialize(format: :short)
      end
    end
  end
end
