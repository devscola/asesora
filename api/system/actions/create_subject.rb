require_relative '../services/organizations/service'
require_relative '../services/solicitudes/service'
require_relative '../services/subjects/service'
require_relative '../services/attachments/service'

module Actions
  class CreateSubject
    class << self
      def do(solicitude_id:, proposal:, description:, analysis:, topics:, created:, project:, files:)
        email = user_email(solicitude_id)
        numeration = next_numeration(email)
        subject = ::Subjects::Service.create(solicitude_id, proposal, description, analysis, topics, created, project, numeration)
        add_files(subject['id'], files)

        subject['files'] = attachments_for(subject['id'])
        subject
      end

      private

      def add_files(subject_id, files)
        if files
          files.each do |file|
            ::Attachments::Service.create(file, subject_id)
          end
        end
      end

      def attachments_for(subject_id)
        ::Attachments::Service.retrieve(subject_id)
      end

      def user_email(solicitude)
        solicitude = ::Solicitudes::Service.retrieve(solicitude)
        email = solicitude['user']['user_id']

        email
      end

      def next_numeration(email)
        numeration = ::Organizations::Service.next_subject_numeration(email)

        numeration
      end
    end
  end
end
