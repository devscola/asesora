require_relative '../services/solicitudes/service'
require_relative '../services/attachments/service'
require_relative './subject_presenter'

module Actions
  class RetrieveAllSubjectsByDomain
    def self.do(domain:)
      result = []
      solicitudes = ::Solicitudes::Service.all_by_domain(domain)

      solicitudes.each do |solicitude|
        company = ::Companies::Service.retrieve(solicitude['company'], solicitude['edition_moment'])
        subjects = ::Subjects::Service.all_by(solicitude['creation_moment'])
        applicant = ::Applicant::Service.retrieve(solicitude['applicant'])

        subjects.each do |subject|
          attachments = ::Attachments::Service.retrieve(subject['id'])
          subject["files"] = attachments

          item = SubjectPresenter.new(solicitude, subject, applicant, company).serialize
          result << item
        end
      end
      result
    end
  end
end
