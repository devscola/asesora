import Solicitude from './pages/Solicitude'
import Fixtures from './Fixtures'

describe('Contact', () => {
  beforeEach(() => {
    Fixtures.pristine()
  })

  after(() => {
    Fixtures.clean()
  })

  it('can be autofilled with a suggestion', () => {
    const contactName = 'Piedad'
    const contactPhone = '000222000'
    const solicitude = new Solicitude()
      .applicantName(contactName)
      .selectFirstContactSuggestion()

    expect(solicitude.hasPhoneNumberInContact(contactPhone)).to.eq.true
  })
})
