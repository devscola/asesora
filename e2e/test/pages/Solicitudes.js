
const COUNT_FIXTURES_MY_SOLICITUDES = 2
const COUNT_FIXTURES_MY_ORGANIZATION_SOLICITUDES_LESS_MINE = 1
const SOLICITUDE_LIST_URL = '/#/solicitudes-list'

class Solicitudes {
  constructor() {
    cy.visit(SOLICITUDE_LIST_URL)
    cy.contains('Mostrar')
    cy.reload(true)
    cy.contains('Mostrar')
    this.removedSolicitudes = 0
  }

  editFirst() {
    cy.get('.solicitude-edit-button').first().click({force: true})
    cy.contains('Solicitante')
    cy.reload(true)
    cy.contains('Solicitante')

    return this
  }

  showFirst() {
    cy.get('.solicitude-show-button').first().click({force: true})
    cy.contains('Solicitante')
    cy.reload(true)
    cy.contains('Solicitante')

    return this
  }

  fill() {
    return this
  }

  filter(criteria){
    cy.get('#select-list input').click({force: true}).type(criteria, {force:true}).type('{enter}', {force:true})
  }

  filterByMyOrganizationLessMine(){
    this.filter('Sólo')

    return this
  }

  filterByMyOrganizationWithMine(){
    this.filter('Mías +')

    return this
  }

  hasNotEditButton(){
    cy.get('#solicitude-edit-button').should('not.exist')

    return true
  }

  description(text) {
    cy.wait(1000)
    cy.get('#solicitude-text').clear()
    cy.get('#solicitude-text').type(text, {force:true})
    cy.get('#solicitude-text').should('have.value', text)

    return this
  }

  editCompany() {
    cy.get('#edit-company').click({force: true})

    return this
  }

  editCompanyNameWith(text) {
    cy.get('#edit-company').click({force: true})
    cy.get('#company-name').type(text, {force: true})
    cy.get('#add-name-value').click({force: true})

    return this
  }

  startSubject() {
    cy.get('#add-subject-button').click({force: true})

    return this
  }

  fillBasicSubject() {
    this.addFirstTopic()
    this.addFirstProposal()

    return this
  }

  closeSubject() {
    cy.get('#close-modal').click({force: true})
    cy.get('#close-counseling').click({force: true})

    return this
  }

  remove() {
    cy.get('#delete-solicitude').click({force: true})
    this.removedSolicitudes ++

    return this
  }

  makeItJustifable() {
    this.addAplicantCCAA()
    this.addSource()
    this.confirmChanges()

    return this
  }

  confirmChanges() {
    cy.get('#submit').click({force: true})
    cy.contains('Mostrar')

    return this
  }

  addAplicantCCAA() {
    cy.get('#applicant-ccaa input').click({force:true}).type('Andalucía', {force:true}).type('{enter}', {force:true})
  }

  addSource() {
    cy.get('#source input').click({force:true}).type('Telefónico', {force:true}).type('{enter}', {force:true})
  }

  addFirstTopic() {
    cy.get('#subjects-topics').click({force:true})
    cy.get('#subjects-topics .item.current').first().click({force: true})
  }

  addFirstProposal() {
    cy.get('#subject-proposals').click({force: true})
    cy.get('#subject-proposals .item.current').first().click({force: true})
  }

  includes(text) {
    cy.contains(text)

    return true
  }

  hasBeenDecreased() {
    const total = COUNT_FIXTURES_MY_SOLICITUDES - this.removedSolicitudes
    cy.get('#solicitudes-list>table>tbody').find('tr').should('have.length', total)

    return true
  }

  notContaisJustified() {
    cy.get('#check').should('not.exist')

    return true
  }

  contaisJustified() {
    cy.get('#check').should('exist')

    return true
  }

  count(quantity){
    cy.get('#solicitudes-list>table>tbody')
      .find('tr')
      .should('have.length', quantity)

    return true
  }
}

export default Solicitudes
