import Subjects from './pages/Subjects'
import Fixtures from './Fixtures'

describe('Subjects', () => {
  beforeEach(() => {
    Fixtures.pristine()
  })

  after(() => {
    Fixtures.clean()
  })

  it('can be created', () => {
    const description = 'New subject description'
    const subjects = new Subjects()
    .editFirstSolicitude()
    .addSubject()
    .createSubject(description)

    expect(subjects.includes(description)).to.eq.true
  })

  it('can be closed', () => {
    new Subjects()
      .editLast()
      .close()
      .chooseReason()


    const subjects = new Subjects().editFirst()
    expect(subjects.includes('Motivo de cierre:')).to.eq.true
  })

  it('can be edited', () => {
    const subjects = new Subjects()
      .editFirst()
      .fillAnalysis('any analysis')
      .confirmChanges()
      .editFirst()

    expect(subjects.includes('Proyecto 1')).to.eq.true
  })

  it.only('can remove its closed status', () => {
    const subjects = new Subjects()
      .editFirst()
      .openSubject()
      .removeClosedStatus()
      .editFirst()

    expect(subjects.includes('Cerrar caso')).to.eq.true
  })

  it('can be deleted', () => {
    const subjects = new Subjects()
      .openFirst()
      .remove()

    expect(subjects.hasBeenRemoved()).to.eq.true
  })

  it('can be justified', () => {
    const subjects = new Subjects()

    expect(subjects.contaisJustified()).to.eq.true
  })

  xit('can be not justified', () => {
    new Subjects()
      .editFirst()
      .removeTopics()
      .confirmChanges()

    const subjects = new Subjects()
    expect(subjects.notContaisJustified()).to.eq.true
  })
})
