import Component from '../infrastructure/component'
import Models from '../models/models'
import view from '../views/linkbar'
import { Bus } from '../bus'
import { askTranslationsFor } from "../infrastructure/translatable"

class Navbar extends Component{

  constructor(){
    super()
  }

  subscribe(){
    Bus.subscribe("got.translation.for.linkbar", this.translate.bind(this))
  }

  askTranslations() {
    askTranslationsFor({ labels: this.data.labels, scope: "linkbar" })
  }

  model(){
    if (!this.data) { this.data = Models.newLinkBar() }

    return this.data
  }

  view() {
    return view
  }

}

const component = new Navbar()

export default Object.assign({}, component.view, { data: component.model })
