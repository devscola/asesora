import Component from '../infrastructure/component'
import Models from '../models/models'
import view from '../views/navbar'
import userSession from '../infrastructure/user_session'
import { Bus } from '../bus'
import { askTranslationsFor } from "../infrastructure/translatable"

class Navbar extends Component{

  constructor(){
    super()
    this.data.values.name = userSession.name()
    this.data.values.imageUrl = userSession.imageUrl()
    this.data.control.isAdmin = userSession.isAdmin()
  }

  subscribe(){
    Bus.subscribe("got.translation.for.navbar", this.translate.bind(this));
  }

  askTranslations() {
    askTranslationsFor({ labels: this.data.labels, scope: "navbar" })
  }

  model(){
    if(!this.data) { this.data = Models.newNavbar() }

    return this.data
  }

  view(){
    return view
  }

}

const component = new Navbar()

export default Object.assign({}, component.view, { data: component.model })
