import _ from 'lodash'
import Component from '../infrastructure/component'
import Models from '../models/models'
import view from '../views/subjects-lot'
import service from '../services/subject'
import userSession from '../infrastructure/user_session'
import { Bus, Channel } from '../bus'
import { askTranslationsFor } from "../infrastructure/translatable"
import { jsonToExcel } from '../infrastructure/json_to_excel'
import { getFilename, getCounselingsFields } from '../lib/counselings'

export class SubjectsLot extends Component {
  constructor(){
    super()
    this.data.userSessionEmail = userSession.email()
    this.channel = Channel('subject')
    service(this.channel).start()

    this.channelSubscribe()
  }

  subscribe(){
    Bus.subscribe("subjects-lot.view.created", this.retrieve.bind(this))
    Bus.subscribe("got.translation.for.subjects-lot", this.translate.bind(this))
    Bus.subscribe("got.counselings", this.downloadCounselings.bind(this))
  }

  channelSubscribe() {
    this.channel.subscribe("got.subjects-list", this.populateSubjectsList.bind(this))
  }

  watchActions(){
    Bus.subscribe('load.solicitude', this.editSolicitude.bind(this))
    Bus.subscribe('show.solicitude', this.showSolicitude.bind(this))
    Bus.subscribe('download.clicked', this.getCounselings.bind(this))
  }

  retrieve(){
    const user_id = userSession.email()
    this.channel.publish("get.subjects-list", user_id)
  }

  editSolicitude(event){
    this.goTo(`/#/solicitude/${ event.detail }`)
  }

  showSolicitude(event){
    this.goTo(`/#/show-solicitude/${ event.detail }`)
  }

  populateSubjectsList({ data }){
    let subjects = []

    data.forEach(element => {
      subjects.push(this.createDataSubjecView(element))
    })
    this.data.subjectsList = subjects

    this.data.hasSubjects = subjects.length > 0
  }

  createDataSubjecView(data) {
    return {
      "solicitude_id": data.solicitude_id,
      "subject_id": data.subject_id,
      "solicitudeDate": data.date,
      "companyName": data.company_name,
      "project": data.project,
      "topics": _.map(data.topics, 'name'),
      "record": {
        "analysis": data.analysis,
        "description": data.description,
        "proposal": data.proposal
      },
      "isSolicitudeJustified": this.isSolicitudeJustified(data.date,
        data.source,
        data.ccaa,
        data.employees,
        data.cnae),
      "isSubjectJustified": this.isSubjectJustified(data.topics, data.proposal, data.project)
    }
  }

  isSolicitudeJustified(
    solicitudeDate,
    source,
    applicantCcaa,
    companyEmployees,
    companyCnae){
    if(solicitudeDate &&
      source &&
      applicantCcaa &&
      companyEmployees &&
      companyCnae
    ){
      return true
    }
    return false
  }

  isSubjectJustified(topics, proposal, project){
    if( topics.length > 0 &&
        proposal.length > 0  &&
        project.text != null){
      return true
    }
    return false
  }

  getCounselings() {
    Bus.publish("get.counselings", userSession.email())
  }

  downloadCounselings({ data: counselings }) {
    const filename = getFilename(userSession.email())
    const counselingsFields = getCounselingsFields(counselings)

    return jsonToExcel(filename, counselingsFields)
  }

  askTranslations() {
    askTranslationsFor({ labels: this.data.labels, scope: "subjects-lot" });
  }

  model(){
    if(!this.data) { this.data = Models.newSubjectLot() }

    return this.data
  }

  view() {
    return view
  }
}

const component = new SubjectsLot()

export default Object.assign({}, component.view, { data: component.model })
