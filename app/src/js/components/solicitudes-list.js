import Component from '../infrastructure/component'
import Service from '../services/solicitudes'
import Models from '../models/models'
import view from '../views/solicitudes-list'
import userSession from '../infrastructure/user_session'
import MaxEmployees from '../lib/MaxEmployees'
import CheckOwnerInfoAvailable from '../lib/checkOwnerAvailable'
import isEmpty from '../lib/isEmpty'
import { Bus } from '../bus'
import { askTranslationsFor } from "../infrastructure/translatable"

class SolicitudesList extends Component {
  constructor(){
    super()

    new Service()
    this.data.userSessionEmail = userSession.email()
    this.buildSelectOptions()
  }

  subscribe(){
    Bus.subscribe("solicitudes-list.view.created", this.retrieve.bind(this))
    Bus.subscribe("got.solicitudes-list", this.populateSolicitudeList.bind(this))
    Bus.subscribe("filtered.my.solicitudes", this.populateSolicitudeList.bind(this))
    Bus.subscribe("filtered.domain.less.mine.solicitudes", this.populateSolicitudeList.bind(this))
    Bus.subscribe("filtered.domain.solicitudes", this.populateSolicitudeList.bind(this))
    Bus.subscribe("got.translation.for.solicitudes-list", this.translate.bind(this))
    Bus.subscribe("got.is.coordinator", this.gotIsCoordinator.bind(this))
  }

  watchActions(){
    Bus.subscribe('load.solicitude', this.loadSolicitude.bind(this))
    Bus.subscribe('show.solicitude', this.showSolicitude.bind(this))
    Bus.subscribe('filter.solicitudes', this.filterSolicitudes.bind(this))
  }

  retrieve(){
    const user_id = userSession.email()
    Bus.publish('get.solicitudes.list', user_id)
  }

  loadSolicitude({ detail }){
    this.goTo(`/#/solicitude/${ detail }`)
  }

  showSolicitude({ detail }){
    this.goTo(`/#/show-solicitude/${ detail }`)
  }

  populateSolicitudeList({ data }){
    this.data.solicitudes = data.map((solicitude) => {
      const userSolicitude = solicitude['user']['user_id']

      solicitude['owner'] = userSession.isCurrentUser(userSolicitude)
      solicitude['justifiedSolicitude'] = this.isJustifiedSolicitude(solicitude)
      solicitude['justifiedSubjects'] = this.countJustifiedSubjects(solicitude.subjects)
      solicitude['titleForJustifiedSubjects'] = this.generateTitleForJustifiedSubjects(solicitude.subjects)

      return solicitude
    })
    let user = userSession.email()
    Bus.publish('get.is.coordinator', user)
  }

  gotIsCoordinator(payload){
    this.data.solicitudes = this.data.solicitudes.map((solicitude) => {
      let request = {
        owner: solicitude['user']['user_id'],
        logged: userSession.email(),
        isCoordinator: payload.data
      }
      solicitude['showOwner'] = CheckOwnerInfoAvailable.do(request)
      return solicitude
    })
  }

  isJustifiedSolicitude(solicitude){
    const numberEmployees = solicitude.company_employees

    const hasCorrectNumberOfEmployees = MaxEmployees.isBelow(numberEmployees)
    const hasDate = !isEmpty(solicitude.date)
    const hasCnae = !isEmpty(solicitude.company_cnae)
    const hasEmployees = !isEmpty(solicitude.company_employees)
    const hasSource = !isEmpty(solicitude.source)
    const hasCcaa = !isEmpty(solicitude.ccaa)

    return (hasDate &&
      hasCnae &&
      hasEmployees &&
      hasSource &&
      hasCcaa &&
      hasCorrectNumberOfEmployees
    )
  }

  countJustifiedSubjects(subjects){
    if (isEmpty(subjects)) return 0

    let justified = 0
    for(let subject of subjects){
      if(!isEmpty(subject.topics) && !isEmpty(subject.proposal) ) {
          justified = justified + 1
      }
    }
    return justified
  }

  generateTitleForJustifiedSubjects(subjects){
    const title = ''
    if (subjects){
      return this.countJustifiedSubjects(subjects) + this.data.labels.of + subjects.length + this.data.labels.justifiedSubjects
    }

    return title
  }

  buildSelectOptions(){
    const domain = this.retrieveDomain()
    const mySolicitudes = this.data.labels.mySolicitudes
    const myDomain = this.data.labels.onlyDomain + domain
    const allSolicitudes = this.data.labels.allSolicitudes + domain

    this.data.listOptions =[
      {"value": 0, "text": mySolicitudes },
      {"value": 1, "text": myDomain},
      {"value": 2, "text": allSolicitudes}]

    this.data.values.selectedList =  this.data.listOptions[0]
  }

  retrieveDomain() {
    const email = userSession.email()
    const domain = email.substring(email.lastIndexOf("@") +1)
    return domain
  }

  filterSolicitudes(){
    const user_id = userSession.email()
    const option = this.data.values.selectedList.value

    if(option == 0) Bus.publish('filter.my.solicitudes', user_id )
    if(option == 1) Bus.publish('filter.domain.less.mine.solicitudes', user_id)
    if(option == 2) Bus.publish('filter.domain.solicitudes', user_id)
  }

  askTranslations() {
    askTranslationsFor({ labels: this.data.labels, scope: "solicitudes-list" });
  }

  model(){
    if(!this.data) { this.data = Models.newSolicitudesList() }

    return this.data
  }

  view() {
    return view
  }
}

const component = new SolicitudesList()

export default Object.assign({}, component.view, { data: component.model })
