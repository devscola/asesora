
export default class Model {
  setValues(key, value) {
    this.values[key] = value
  }

  set(key, value) {
    this[key] = value
  }
}
