import {Bus} from '../bus'

export default function buildCallback(signal) {
  return function(response) {
    Bus.publish(signal, response)
  }
}
