import XLSX from 'xlsx'

export const jsonToExcel = (filename = 'asesoramientos.xls', data = {}) => {
  const sheet = XLSX.utils.json_to_sheet(data)
  const workbook = XLSX.utils.book_new()

  XLSX.utils.book_append_sheet(workbook, sheet, 'asesoramientos')
  console.log(XLSX.writeFile(workbook, `${filename}.xlsx`))

  return data
}
