export default class CheckOwnerInfoAvailable {

  static do(payload){
    return this.isShowOwner(payload.owner, payload.logged, payload.isCoordinator)
  }

  static isShowOwner(owner, logged, isCoordinator){
    if (this.belongsToIstas(logged)) return true

    if (this.belongsToSameDomain(owner, logged) && isCoordinator) return true
    return false
  }

  static belongsToIstas(logged){
    return logged.includes("istas.ccoo.es")
  }

  static belongsToSameDomain(owner, logged){
    const domainOwner = this.extractDomain(owner)
    const domainLogged = this.extractDomain(logged)
    return domainOwner == domainLogged
  }

  static extractDomain(email){
    return email.substring(email.lastIndexOf("@") +1)
  }
}
