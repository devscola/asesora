export const isValidEmail = email => {
  if (email == "") { return false }

  const EMAIL_PATTERN = /^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$/
  return EMAIL_PATTERN.test(email)
}

export const isValidCif = cif => {
  if (!cif) return false
  
  cif = cif.toUpperCase().replace(/[\s\-]+/g,'');

  if(/^(\d|[XYZ])\d{7}[A-Z]$/.test(cif)) {
    var num = cif.match(/\d+/);
    num = (cif[0]!='Z'? cif[0]!='Y'? 0: 1: 2)+num;
    if(cif[8]=='TRWAGMYFPDXBNJZSQVHLCKE'[num%23]) {
      return true
    }
  }
  else if(/^[ABCDEFGHJKLMNPQRSUVW]\d{7}[\dA-J]$/.test(cif)) {
    for(var sum=0,i=1;i<8;++i) {
      var num = cif[i]<<i%2;
      var uni = num%10;
      sum += (num-uni)/10+uni;
    }
    var c = (10-sum%10)%10;
    if(cif[8]==c || cif[8]=='JABCDEFGHI'[c]) {
      return true
    }
  }
  return false;
}
