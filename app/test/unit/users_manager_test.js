import { shallowMount } from '@vue/test-utils'
import UserManagerView from '../../src/js/views/users-manager'
import EmailFieldView from '../../src/js/views/email-field'
import UsersListView from '../../src/js/views/users-list'

describe('User\'s manager', () => {
  let wrapper;

  before(function(){
    const mocks = {
      labels: { title: "" },
      values: { emails: [] },
      control: { 
        hasError: false,
        isValidEmail: false
      }
    };

    wrapper = shallowMount(UserManagerView, { mocks });
  });

  it('has an email field', function() {
    expect( wrapper.find(EmailFieldView).exists() ).to.eq(true)
  });
  
  it('has a list of users', function() {
    expect( wrapper.find(UsersListView).exists() ).to.eq(true)
  });

})
