import { shallowMount } from '@vue/test-utils'
import SubjectsList from '../../src/js/views/solicitude/subject/subject-cards-list.vue'
import Models from '../../src/js/models/models'

describe('Subject cards list', () => {
  let propsDefault

  beforeEach(function(){
    propsDefault = Object.assign({}, Models.newShowSolicitude())
  })

  it('add class when subject closed', () => {
    const withSubjectClosed = Object.assign({}, propsDefault, {
      values: {
        subjects: [{ closed: 'a_moment' }]
      }
    })
    const wrapper = shallowMount(SubjectsList, { propsData: withSubjectClosed })
    expect(wrapper.find('.closedSubject').exists()).to.eq(true)
  })

  it('renders a comment', () => {
    const comments = 'a_comment'
    const withSubjectComments = Object.assign({}, propsDefault, {
      values: {
        subjects: [{ comments }]
      }
    })
    const wrapper = shallowMount(SubjectsList, { propsData: withSubjectComments })
    expect(wrapper.text()).to.contain(comments)
  })

  it('renders an analysis', () => {
    const analysis = 'An analysis'
    const withAnalysis = Object.assign({}, propsDefault, {
      values: {
        subjects: [{ analysis }]
      },
      editionSubject: 'none'
    })
    const wrapper = shallowMount(SubjectsList, { propsData: withAnalysis })
    expect(wrapper.text()).to.contain(analysis)
  })

  it('renders the numeration of the subject', () => {
    const numeration = 'domain.com-1'
    const withNumeration = Object.assign({}, propsDefault, {
      values: {
        subjects: [{ numeration }]
      },
      editionSubject: 'none'
    })
    const wrapper = shallowMount(SubjectsList, { propsData: withNumeration })
    expect(wrapper.text()).to.contain(numeration)
  })

})
