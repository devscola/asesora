import { shallowMount } from '@vue/test-utils'
import ShowSolicitude from '../../src/js/views//show-solicitude.vue'
import SummarySolicitudeView from '../../src/js/views/solicitude/subject/subject-summary-solicitude'
import ButtonAddSubject from '../../src/js/views/custom-button'
import SubjectCardsListView from '../../src/js/views/solicitude/subject/subject-cards-list'
import Models from '../../src/js/models/models'

describe('Show solicitude', () => {
  let $route, mocks
  beforeEach(function(){
    $route = { params: { id : '' } }
    mocks = Object.assign({}, Models.newShowSolicitude(), { $route })
  })

  it('has a summary of solicitude', function(){
    const wrapper = shallowMount(ShowSolicitude, { mocks })

    expect( wrapper.find(SummarySolicitudeView).exists() ).to.eq(true)
  })

  it('has a button to add subject', function(){
    const withButtonsPresent = Object.assign({}, mocks, { buttonsPresent: true, owner: true, organization: true })
    const wrapper = shallowMount(ShowSolicitude, { mocks: withButtonsPresent })
    expect( wrapper.find(ButtonAddSubject).exists() ).to.eq(true)
  })

  it('has a subject list', function(){
    const withSubjects = Object.assign({}, mocks, { hasSubjects: true })
    const wrapper = shallowMount(ShowSolicitude, { mocks: withSubjects })

    expect( wrapper.find(SubjectCardsListView).exists() ).to.eq(true)
  })

  it('calls solicitude edit on edit-button click', function(){
    const clickSpy = sinon.spy()
    const withButtonsPresent = Object.assign({}, mocks, { buttonsPresent: true, owner: true, organization: true })
    const methods = { solicitudeEdit: clickSpy }
    const wrapper = shallowMount(ShowSolicitude, { mocks: withButtonsPresent, methods })

    wrapper.find('#edit-button').trigger('click')

    expect(clickSpy).to.be.calledOnce
  })

})
