import { Solicitude } from "../../../src/js/components/solicitude"
import { APIClient } from "../../../src/js/infrastructure/api_client"

describe('Solicitude', () => {
  var apiClient

  beforeEach(() => {
    apiClient = sinon.stub(APIClient, 'hit')
  })

  afterEach(() => {
    apiClient.restore()
  })

  it('is justifiable when required values are correctly filled', () => {
    const solicitude = createJustificableSolicitude()

    expect(solicitude.data.warning).be.eq(false)
  })

  function createJustificableSolicitude(){
    const solicitude = new Solicitude()

    const assignations = {
      companyEmployees: '2',
      companyCnae: '101',
      source: {text: "text"},
      applicantCcaa: {text: "text"}
    }

    for (const key in assignations) {
      solicitude.data.values[key] = assignations[key]
      solicitude.warningRequired()
      expect(solicitude.data.warning).be.eq(true)
    }

    solicitude.data.values['date'] = "11/11/11"
    solicitude.warningRequired()
    expect(solicitude.data.warning).be.eq(false)
    return solicitude
  }
})
