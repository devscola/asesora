import { shallowMount } from '@vue/test-utils'
import view from '../../src/js/views/custom-button'

describe('Custom button', () => {
  let propsDefault;
  
  before(function(){
    propsDefault = {
      label: ""
    };
    
  });
  
  it('has a label', function() {
    const props = Object.assign({}, propsDefault, { label: "my button" })
    const wrapper = shallowMount(view, { propsData : props })
    const $button = wrapper.find("button")
    
    expect($button.text()).to.eq("my button")
  });
  
  it('handles a click', function() {
    const clickSpy = sinon.spy()
    const props = Object.assign({}, propsDefault)
    const methods = { handleClick: clickSpy }
    const wrapper = shallowMount(view, { propsData: props, methods })

    wrapper.find("button").trigger('click')
    
    expect(clickSpy).to.be.calledOnce
  })

  it('can be disabled', () => {

  })

})
