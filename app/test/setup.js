require('jsdom-global')()
require('mock-local-storage')

window.console = global.console
window.sessionStorage = global.sessionStorage

global.expect = require('chai').expect
global.assert = require('chai').assert
global.sinon = require('sinon')
global.window = document.defaultView

var chai = require('chai')
var sinonChai = require('sinon-chai')

chai.use(sinonChai)